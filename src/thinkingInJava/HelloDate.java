package thinkingInJava;

import java.util.Date;
/** The first thinking in java example program.
 * Display a string and today's date.
 * @author Shawn Liu
 * @author www.shawn.xin
 * @version 4.0
 * 
 * 
 * */

public class HelloDate {
	/**Entry point to class & application
	 * @param args array of string arguments
	 * @throws exception no exceptions thrown
	 * */
	public static void main(String[] args) {
		System.out.println("hello, it's:");
		System.out.println(new Date());
	}
}
/* Output:(55% match)
 * hello, it's:
 * Sat Mar 31 16:03:49 CST 2018
 * */
